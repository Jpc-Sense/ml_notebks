import pandas as pd

class FeatureSetUpdate(object):

    def __init__(self, leak_df, observations):
        self.leak_df = leak_df
        self.observations = observations

    @staticmethod
    def _get_electricity_data():
        try:
            pass
            # retrieve electricity data from energidatabasen
        except:
            pass
            # If the api is down, send back a message

    @staticmethod
    def _get_drought_data():
        try:
            pass
            # retrieve drought data from energidatabasen
        except:
            pass
            # If the api is down, send back a message

    @staticmethod
    def _get_rain_data():
        try:
            pass
            # retrieve rain data from energidatabasen
        except:
            pass
            # If the api is down, send back a message

    @staticmethod
    def _get_wd_data():
        pass
        # retrieve from the class already made

    @staticmethod
    def _get_hour_data():
        pass
        # just do the df['hour'] = df.index.hour on the latest data

    def _calc_new_observations(self):
        current_leak = np.mean(self.leak_df.values[-35:])
        latest_obs = self.observations.iloc[-24: , :]
        deleaked_obs = latest_obs - current_leak
        return deleaked_obs

    @classmethod
    def create_new_feature_df(cls):
        el = cls._get_electricity_data()
        dr = cls._get_drought_data()
        ra = cls._get_rain_data()
        wd = cls._get_wd_data()
        ho = cls._get_hour_data()
        ob = cls._calc_new_observations()
        return pd.concat([ob, el, dr, ra, wd, ho, ob], axis=1)
