import numpy as np

class LeakPrediction(object):

    MODEL = ##read model.keras file

    def __init__(self, feature_set, observations):
        self.feature_set = feature_set
        self.observations = observations

    def _predict_24h(self):
        obs = self.observations.iloc[-24:, :]
        preds = MODEL.pred(obs)
        return preds

    def _calc_leak(self):
        preds = self._predict_24h()
        leak = np.mean(obs - preds)
        return leak

    def push_pred(self):
        leak_pred = self._calc_leak()
        ## Handle how to paste a datetime on it as well
